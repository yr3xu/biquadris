#include "board.h"
#include <iostream> 
#include <fstream>
#include <cstdlib>   
using namespace std; 


/*********** subject ************/
Info Board::getInfo() const{
  if (isBlind && initBlind) {
    return Info{getLevel(), getScore(), 0, 'B', false, false, true, nextBlock}; 
  } else if (isBlind && !initBlind) {
    return Info{getLevel(), getScore(), 0, 'B', true, false, false, nextBlock}; 
  } else {
    return Info{getLevel(), getScore(), 0, 'B', false, false, false, nextBlock}; 
  }
}



/******** Initializers ********/ 

Board::Board(int start_level)
  : level{start_level} {}

Board::~Board(){
  for (size_t i = 0; i < activeBlocks.size(); i++) {
    delete activeBlocks.at(i);
  }
  delete td;
  delete ob;
  delete l;
}

void Board::init(size_t board_cols, size_t board_rows, bool only_text){
  l = nullptr;
  this->only_text = only_text;

  td = new TextDisplay(board_cols, board_rows);
  if (only_text) {
    ob = nullptr;
  } else {
    ob = new GraphicsDisplay(board_cols, board_rows);  
  }

  for (size_t i = 0; i < board_rows; i++){
    vector <Cell> grid_row; 
    for (size_t j = 0; j < board_cols; j++){
      grid_row.emplace_back(Cell{i, j});
    }
    theBoard.emplace_back(grid_row); 
  }
  setLevelPointer(); 
  currBlock = l->genBlock();
  nextBlock = l->genBlock();
  activeBlocks.emplace_back(currBlock);
  activeBlocks.emplace_back(nextBlock);
  draw(currBlock);
}

/********** Block movement  ********/ 



void Board::moveRight(size_t times) {
  undraw(currBlock); 
  vector<Coord> copy = currBlock->getAbsoluteLoc(); 
  for (size_t i = 0; i < times; i++){
    currBlock->moveRight();
    if (!canMoveTo(currBlock->getAbsoluteLoc())) {
      currBlock->setAbsPos(copy); 
      break; 
    }
  }
  copy = currBlock->getAbsoluteLoc(); 
  //TO DO make this into move heavy
  if (isHeavy()) {
    if (getLevel() == 3 || getLevel() == 4) {
      // if heavy and in level 3 or 4, the effect is cumulative. i.e 3 drops
      currBlock->moveDown();
      currBlock->moveDown();
      currBlock->moveDown();
    } else {
      // if heavy but in level 0 or 1 or 2, drop 2
      currBlock->moveDown();
      currBlock->moveDown();
    }
    if (!canMoveTo(currBlock->getAbsoluteLoc())) {
      // if not possible to move down 2 or 3 times, then drop block
      currBlock->setAbsPos(copy); 
      draw(currBlock);
      dropBlock();
      return;
    }
  } else if (getLevel() == 3 || getLevel() == 4) {
    // else if levels 3 or 4, but not heavy, then move down only one
    currBlock->moveDown();
    if (!canMoveTo(currBlock->getAbsoluteLoc())) {
      currBlock->setAbsPos(copy); 
    }
  }
  draw(currBlock);
}

void Board::moveLeft(size_t times) {
  undraw(currBlock); 
  vector<Coord> copy = currBlock->getAbsoluteLoc(); 
  for (size_t i = 0; i < times; i++){
    currBlock->moveLeft();
    if (!canMoveTo(currBlock->getAbsoluteLoc())) {
      currBlock->setAbsPos(copy); 
      break; 
    }
  }
  copy = currBlock->getAbsoluteLoc(); 
  if (isHeavy()) {
    if (getLevel() == 3 || getLevel() == 4) {
      // if heavy and in level 3 or 4, the effect is cumulative. i.e 3 drops
      currBlock->moveDown();
      currBlock->moveDown();
      currBlock->moveDown();
    } else {
      // if heavy but in level 0 or 1 or 2, drop 2
      currBlock->moveDown();
      currBlock->moveDown();
    }
    if (!canMoveTo(currBlock->getAbsoluteLoc())) {
      // if not possible to move down 2 or 3 times, then drop block
      currBlock->setAbsPos(copy); 
      draw(currBlock);
      dropBlock();
      return;
    }
  } else if (getLevel() == 3 || getLevel() == 4) {
    // else if levels 3 or 4, but not heavy, then move down only one
    currBlock->moveDown();
    if (!canMoveTo(currBlock->getAbsoluteLoc())) {
      currBlock->setAbsPos(copy); 
    }
  }
  draw(currBlock);
}

void Board::moveDown(size_t times) {
  undraw(currBlock); 
  vector<Coord> copy = currBlock->getAbsoluteLoc(); 
  for (size_t i = 0; i < times; i++){
    currBlock->moveDown();
    if (!canMoveTo(currBlock->getAbsoluteLoc())) {
      currBlock->setAbsPos(copy); 
      break; 
    }
  }
  copy = currBlock->getAbsoluteLoc(); 
  if (isHeavy()) {
    if (getLevel() == 3 || getLevel() == 4) {
      // if heavy and in level 3 or 4, the effect is cumulative. i.e 3 drops
      currBlock->moveDown();
      currBlock->moveDown();
      currBlock->moveDown();
    } else {
      // if heavy but in level 0 or 1 or 2, drop 2
      currBlock->moveDown();
      currBlock->moveDown();
    }
    if (!canMoveTo(currBlock->getAbsoluteLoc())) {
      // if not possible to move down 2 or 3 times, then drop block
      currBlock->setAbsPos(copy); 
      draw(currBlock);
      dropBlock();
      return;
    }
  } else if (getLevel() == 3 || getLevel() == 4) {
    // else if levels 3 or 4, but not heavy, then move down only one
    currBlock->moveDown();
    if (!canMoveTo(currBlock->getAbsoluteLoc())) {
      currBlock->setAbsPos(copy); 
    }
  }
  draw(currBlock);

}

void Board::rotateCw(size_t times){
  times %= 4; 
  undraw(currBlock); 
  vector<Coord> copy = currBlock->getAbsoluteLoc(); 
  for (size_t i = 0; i < times; i++){
    currBlock->rotateCw();
    if (!canMoveTo(currBlock->getAbsoluteLoc())) {
      currBlock->setAbsPos(copy); 
      break; 
    }
  }
  copy = currBlock->getAbsoluteLoc(); 
  if (isHeavy()) {
    if (getLevel() == 3 || getLevel() == 4) {
      // if heavy and in level 3 or 4, the effect is cumulative. i.e 3 drops
      currBlock->moveDown();
      currBlock->moveDown();
      currBlock->moveDown();
    }
    if (!canMoveTo(currBlock->getAbsoluteLoc())) {
      // if not possible to move down 2 or 3 times, then drop block
      currBlock->setAbsPos(copy); 
      draw(currBlock);
      dropBlock();
      return;
    }
  } else if (getLevel() == 3 || getLevel() == 4) {
    // else if levels 3 or 4, but not heavy, then move down only one
    currBlock->moveDown();
    if (!canMoveTo(currBlock->getAbsoluteLoc())) {
      currBlock->setAbsPos(copy); 
    }
  }
  draw(currBlock);
}

void Board::rotateCcw(size_t times){
  rotateCw(4-times%4);
}

/*********** Helpers for movement ************/

bool Board::inBounds(int c, int r, int max_width, int max_height){
  return (c >= 0 && c < max_width && r >= 0 && r < max_height); 
}

bool Board::isEmptyCell(int r, int c){
  return (theBoard.at(r).at(c).getInfo().type == '.'); 
}

/******** Helpers for Dropping/Clearing/Scoring *********/ 

bool Board::canMoveTo(vector<Coord> c){
  for (size_t i = 0; i < c.size(); i++) {
    if (!inBounds(c.at(i).x, c.at(i).y, theBoard.at(0).size(),theBoard.size()) || !isEmptyCell(c.at(i).y, c.at(i).x)) {
      return false;
    } 
  }
  return true;
}

bool Board::rowFull(int r){
  for (size_t c = 0; c < theBoard.at(r).size(); c++){
    if (isEmptyCell(r, c)){
      return false; 
    }
  }
  return true; 
}

void Board::checkFullRows(){
  int rows_cleared = 0;
  bool one_row_cleared = false;
  for (size_t r = 0; r < theBoard.size(); r++){
    if (rowFull(r)){
      rowsCleared++;
      clearRow(r); 
      for (size_t i = 0; i < activeBlocks.size(); i++) {
        if(activeBlocks.at(i)->getAbsoluteLoc().size() == 0) {
          delete activeBlocks.at(i);
          activeBlocks.erase(activeBlocks.begin() + i);
        }
      }

      vector<int> validator;
      for (size_t i = 0; i < activeBlocks.size(); i++) {
        validator.emplace_back(0);
      }
      for (int i = r; i > 0; i--) {
        collapseRows(i - 1, validator);
      }
      rows_cleared++;
      one_row_cleared = true;
    }
  }

  if (one_row_cleared) {
    score += (rows_cleared + getLevel()) * (rows_cleared + getLevel());
  }

  if (rows_cleared == 0 && getLevel() == 4) {
    fork_counter += 1;
  } else if (rows_cleared > 0 && getLevel() == 4) {
    fork_counter = 0;
  } 
}
/************* Dropping / Clearing / Scoring *******/

void Board::dropBlock() {
  //move down by 1 cell until you can't anymore. 
  for (int r = 0; r < theBoard.size(); r++) {
    vector<Coord> currCopy = currBlock->getAbsoluteLoc(); 
    undraw(currBlock); 
    currBlock->moveDown();
    
    if (!canMoveTo(currBlock->getAbsoluteLoc())) {
      currBlock->setAbsPos(currCopy); 
      draw(currBlock);
      break;
    } 
    draw(currBlock);
  }
  draw(currBlock);
  checkFullRows(); // clear
  unsetBlind();
  this->notifyObservers();

  // check for fork piece logic
  if (fork_counter % 5 == 0 && fork_counter != 0 && getLevel() == 4) {
    Block* xblock = new XBlock(4);
    activeBlocks.emplace_back(xblock);
    for (size_t i = 0; i < theBoard.size(); i++) {
      vector<Coord> xcopy = xblock->getAbsoluteLoc(); 
      undraw(xblock); 
      xblock->moveDown();
      
      if (!canMoveTo(xblock->getAbsoluteLoc())) {
        xblock->setAbsPos(xcopy); 
        draw(xblock);
        break;
      }
      draw(xblock);
    }
    fork_counter = 0;
  }
  
  generateNewBlock(); 
  toggleDropped();
}


void Board::clearRow(int r) {
  int width = theBoard.at(0).size();
  //begin clearing row
  for (int j = 0; j < width; j++) {
    if (theBoard.at(r).at(j).getObSize() == 1) {
      score += (theBoard.at(r).at(j).getInfo().level + 1) * (theBoard.at(r).at(j).getInfo().level + 1);
    }
    Coord self_remove{j, r};
    theBoard.at(r).at(j).getInfo().block->removeCoords(self_remove);
    theBoard.at(r).at(j).setCell(Info{-1, 0, 0, '.', false, true, false, nullptr});
    theBoard.at(r).at(j).notifyObservers();
    
    theBoard.at(r).at(j).detachAll();
    theBoard.at(r).at(j).setCell(Info{-1, 0, 0, '.', false, false, false, nullptr});
  }
}

// goes through each cell in the row to be cleared, and move the block that the cell has down by 1
// via theCell->block->moveDown
void Board::collapseRows(int r, vector<int>& validator) {
  int width = theBoard.at(0).size();
  /*
  First iteration: if any block on the row to be cleared cannot be moved down, either:
  it has been cut in half by row clear:
  I
  I Z
  X X X X X X X X X X X X X
  I X Z
  - - - - - - - - - - <- bottom of board
  
  or:

  an adjacent block is preventing it from being moved down

  X S S J       T T T
  S S J J         T I I I I
  X X X X X X X X X X X X X
  - - - - - - - - - - - -

  Then we skip that cell and move on to the next one. Because in the row there will always be at least one block, or 
  whatever of it is left, that is collapsable into the cleared row. When this has been resolved, we iterate through
  the row again and lower every OTHER block that has not yet been lowered.

  Note: 
  Since collapseRows is done for every row above the cleared row, so as to, eventually, lower every cell on board down by one 
  above the row that's being cleared, every block in the board above the collapsed row will be lowered ONCE and only ONCE.
  This is validated through vector<int>& validator 
  */
  for (int i = 0; i < width; i++) {
    // only on empty cells
    if (theBoard.at(r).at(i).getInfo().type != '.') {
      Block* to_be_moved = theBoard.at(r).at(i).getInfo().block;
      // check if that active block on the board has already been lowered by collapseRows
      for (size_t j = 0; j < activeBlocks.size(); j++) {
        if (to_be_moved == activeBlocks.at(j) && validator.at(j) == 0) {
          undraw(to_be_moved);
          vector<Coord> copy = to_be_moved->getAbsoluteLoc(); 
          to_be_moved->moveDown();
          for(size_t k = 0; k < to_be_moved->getAbsoluteLoc().size(); k++) {
          }
          if (!canMoveTo(to_be_moved->getAbsoluteLoc())) {
            // move down not possible, nothing is done, and we move onto the next cell
            to_be_moved->setAbsPos(copy); 
          } else {
            // move down is possible, and we flag that block as lowered
            validator.at(j) = 1;
          }
          draw(to_be_moved);
        }
      }
    }
  }

  // Second iteration: any remaining blocks on row r will be lowered once, excluding those that have already been lowered
  // by the previous iteration
  for (int i = width - 1; i >= 0; i--) {
    // only on empty cells
    if (theBoard.at(r).at(i).getInfo().type != '.') {
      Block* to_be_moved = theBoard.at(r).at(i).getInfo().block;
      // check if that active block on the board has already been lowered by collapseRows
      for (size_t j = 0; j < activeBlocks.size(); j++) {
        if (to_be_moved == activeBlocks.at(j) && validator.at(j) == 0) {
          undraw(to_be_moved);
          vector<Coord> copy = to_be_moved->getAbsoluteLoc(); 
          to_be_moved->moveDown();
          for(size_t k = 0; k < to_be_moved->getAbsoluteLoc().size(); k++) {
          }
          // Now any block will be moved regardless. There will be no more cases of colliding blocks preventing the movement of this one
          // If movement is hindered, it will be because of slicing
          if (!canMoveTo(to_be_moved->getAbsoluteLoc())) {
            for (size_t k = 0; k < copy.size(); k++) {
              if (copy.at(k).y < r + 1) {
                copy.at(k).y += 1;
              }
            }
            to_be_moved->setAbsPos(copy); 
          }
          validator.at(j) = 1;
          draw(to_be_moved);
        }
      }
    }
  }
}

/******** Next Block Generation (based on levels) *******/

void Board::generateNewBlock(){
  // if next piece cannot be place at top of board, game is O V E R 
  currBlock = nextBlock;
  if (!canMoveTo(currBlock->getAbsoluteLoc())){
    this->lost = true; 
    cout << "GAMEOVER" << endl;
    return; 
  }
  
  nextBlock = l->genBlock();
  activeBlocks.emplace_back(nextBlock);

  // generate and draw new next block at bottom of board 
  draw(currBlock);

}

/********* Getters and Setters ******/

void Board::setBlind(){
  isBlind = true; 
  initBlind = true;
  this->notifyObservers();
  initBlind = false;
}
void Board::unsetBlind(){
  initBlind = true;
  this->notifyObservers();
  isBlind = false;
  initBlind = false;
}


void Board::setRandom(){
  isRandom = true; 
}
void Board::unsetRandom(){
  isRandom = false; 
}

void Board::setCurrBlock(int level, char type){
  undraw(currBlock);     //sets and draws  

  for (size_t i = 0; i < activeBlocks.size(); i++) {
    if(activeBlocks.at(i) == currBlock) {
      delete activeBlocks.at(i);
      activeBlocks.erase(activeBlocks.begin() + i);
    }
  }
   if (type == 'S'){
        currBlock = new SBlock(level);
    } else if (type == 'Z'){
        currBlock = new ZBlock(level);
    } else if (type == 'O'){
        currBlock = new OBlock(level);
    } else if (type == 'L'){
        currBlock = new LBlock(level);
    } else if (type == 'J'){
        currBlock = new JBlock(level);
    } else if (type == 'T'){
        currBlock = new TBlock(level);
    } else if (type == 'I'){
        currBlock = new IBlock(level);
    } else {
      return; 
    }
    activeBlocks.emplace_back(currBlock);
    draw(currBlock);
}

void Board::setLevelPointer(){
  delete l;
  l = nullptr;
  switch(level){
    case 0: 
      l = new LevelZero(block_seq); 
      return;
    case 1: 
      l = new LevelOne(seed); 
      return;
    case 2:
      l = new LevelTwo(seed); 
      return; 
    case 3:
      l = new LevelThree(seed, block_seq, isRandom); 
      return; 
    case 4:
      l = new LevelFour(seed, block_seq, isRandom);
      return; 
    default: 
      return;
  }
}

void Board::levelup(size_t times){
  if (getLevel() + times <= 4) {
    level += times;
    if (this->getObSize() > 0) {
      this->notifyObservers();
    }
  }
}

void Board::leveldown(size_t times){
  int amount = times;
  if (getLevel() - amount >= 0) {
    level -= amount;
    if (this->getObSize() > 0) {
      this->notifyObservers();
    }
  }
}

int Board::getLevel() const{
  return level;
}

int Board::getScore() const{
  return score; 
}

Block * Board:: getNextBlock() const{
  return nextBlock;
}

bool Board::gameOver() const{
  return lost; 
}

bool Board::isHeavy() const {
  return heavy;
}

void Board::toggleHeavy() {
  if (heavy) {
    heavy = false;
  } else {
    heavy = true;
  }
}

void Board::setHeavy() {
  heavy = true;
};

bool Board::getDropped() const {
  return isDropped;
}

bool Board::toggleDropped() {
  if (isDropped == false) {
    isDropped = true;
  } else {
    isDropped = false;
  }
};


void Board::setSeq(std::vector<char> seq){
  block_seq = seq;
}

void Board::resetSeq(string fname){
  char scriptBlock;

  ifstream sf;
  sf.open(fname);

  block_seq.clear();

  while (true) {
    if (sf.eof()) {
      break;
    }
    if (sf >> scriptBlock) {
      block_seq.emplace_back(scriptBlock);
    }
  }
  sf.close();
}

void Board::changeDrops(int changeTo) {
  remainingDrops = changeTo;
}

int Board::getDrops() {
  return remainingDrops;
}


int Board::getRowsCleared(){
  return rowsCleared; 
}

void Board::setRowsCleared() {
  rowsCleared = 0;
}

/******* Drawing/Printing ******/

void Board::printRow(size_t i){
  if (isBlind && i >= (3-1) && i <= (12-1)){
    td->printBlind(i);    // columns 3-9 is '?'
  } else {
    td->printRow(i);
  }
}

void Board::draw(Block* b){
  if (b == nullptr) {
    return;
  }
  vector<Coord> abs_location = b->getAbsoluteLoc();
  for(size_t i = 0; i < abs_location.size(); i++) {
    int sub_row = abs_location.at(i).y;
    int sub_col = abs_location.at(i).x;
    for(size_t j = 0; j < abs_location.size(); j++) {
      int ob_row = abs_location.at(j).y;
      int ob_col = abs_location.at(j).x;
      if (sub_row == ob_row && sub_col == ob_col) {
        continue;
      } else {
        theBoard.at(sub_row).at(sub_col).attach(&theBoard.at(ob_row).at(ob_col));
      }
    }
    theBoard.at(sub_row).at(sub_col).attach(td);
    if (!only_text) {
      this->attach(ob);
      theBoard.at(sub_row).at(sub_col).attach(ob);
    }
  }

  int row = abs_location.at(0).y;
  int col = abs_location.at(0).x;
  theBoard.at(row).at(col).setCell(Info{b->getLevel(), row, col, b->getType(), false, false, false, b});
  theBoard.at(row).at(col).notifyObservers();
}

void Board::undraw(Block* b) {
  if (b == nullptr) {
    return;
  }
  vector<Coord> abs_location = b->getAbsoluteLoc();
  char c = '.';
  theBoard.at(abs_location.at(0).y).at(abs_location.at(0).x).setCell(Info{0, 0, 0, c, false, false, false, nullptr});
  theBoard.at(abs_location.at(0).y).at(abs_location.at(0).x).notifyObservers();

  for(size_t i = 0; i < abs_location.size(); i++) {
    int sub_row = abs_location.at(i).y;
    int sub_col = abs_location.at(i).x;
    theBoard.at(sub_row).at(sub_col).detachAll();
  }
}





