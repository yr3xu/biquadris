#include <iostream>
#include <iomanip> 
#include <fstream>
#include <string>
#include "biquadris.h"
using namespace std;

/****** Initializers ******/ 

void Biquadris::play(int num_players, int argc, char *argv[]){
  this->num_players = num_players;
  readArgs(argc, argv);
  populateScripts();
  init();   
}

void Biquadris::init(){
  for (size_t i = 0; i < num_players; i++){
    Board* b = new Board(start_level);
    b->setSeq(this->p1p2seq.at(i));
    b->init(board_cols, board_rows, onlyText);
    boards.emplace_back(b); 
  }  
  printGame();
  readCmd(); 
  restart();
}


// fio b etter
void Biquadris::restart(){
  for (size_t i = 0; i < boards.size(); i++){
    delete boards.at(i);
    boards.at(i) = nullptr;    // unqiue ptr so not this  
  }
  boards.clear(); 

  // init();
  curr_player = 0; 
  highscore = 0; 
  isHeavy = false; 
}

/***** Reading cmd line args and stdin cmds ******/

void Biquadris::readArgs(int argc, char *argv[]){
  for (int i = 1; i < argc; i++){
    string cmdin;
    cmdin = {argv[i]};

    if (cmdin == "-text") {
      displayTextOnly(); 
    } 
    
    else if (cmdin == "-seed") {
      istringstream ss(argv[i++]);
      int seedin;
      ss >> seedin;
      if (seed > 0){
        setSeed(seedin);
        srand(seed);
      }
    } 

    else if (cmdin == "-scriptfile1") {
      istringstream ss(argv[i++]); 
      string fname; 
      ss >> fname; 
      setScript1(fname); 
    } else if (cmdin == "-scriptfile2")
    {
      istringstream ss(argv[i++]); 
      string fname; 
      ss >> fname; 
      setScript2(fname);    
    } 
    
    else if (cmdin == "-startlevel") {
      int lvlin;
      istringstream ss(argv[i++]);
      ss >> lvlin; 
      if (lvlin >= 0 && lvlin <= 4)  {
        setStartLevel(lvlin); 
      }
    }
  }
}

void Biquadris::readCmd(){
  curr_player = 0; 
  string user_cmd;

  while (true) {
    //execute drops if there are any remaining before reading commands
    if ((boards.at(curr_player))->getDrops() > 0) {

      executeCmd(boards.at(curr_player)->getDrops(), "drop");

      printGame();

    } else {
        
      cin >> user_cmd;

      if(cin.eof()){
        break;
      }

      int times = ci.parseTimes(user_cmd);
      string cmd = ci.parseCommands(user_cmd, times);

      if (cmd == "sequence") {
        string file;
        cin >> file;

        ifstream myfile;
        myfile.open(file);

        string file_cmd;
        while (true) {
          if (myfile.eof()) {
            break;
          }
        
          if (myfile >> file_cmd) {

            times = ci.parseTimes(file_cmd);
            string new_cmd = ci.parseCommands(file_cmd, times);

            executeCmd(times, new_cmd);
            printGame(); 
          }
        }
        myfile.close();
      }  else if (cmd == "restart") {
          return;
      } else if (!allGamesOver() || (allGamesOver() && cmd == "restart")) {
          executeCmd(times, cmd);
          printGame(); 
      }
    }
  }
}

void Biquadris::executeCmd(int times, string cmd){
  // Which player's turn  
  Board* currboard = boards.at(curr_player);

  if (cmd == "rename") {
    string old_name;
    string new_name;
    cin >> old_name >> new_name;
    ci.rename(old_name, new_name);

  } else if (cmd == "left") {
    currboard->moveLeft(times); 
    
  } else if (cmd == "right") {
      currboard->moveRight(times); 
      
  } else if (cmd == "down") {
      currboard->moveDown(times); 

  } else if (cmd == "clockwise") {
    currboard->rotateCw(times); 

  } else if (cmd == "counterclockwise") {
      currboard->rotateCcw(times); 
      
  } else if (cmd == "drop") {
    if (times > 1) {
      currboard->changeDrops(times - 1);
    }
    currboard->dropBlock();

    currboard->unsetBlind();   // blind unsets itself  after the drop
    if (!(boards.at((curr_player + 1) % (num_players)))->gameOver()) {
      curr_player = (curr_player + 1) % (num_players);
    }

    if(currboard->getRowsCleared() >= 2){ 
      cout << "Enter a special action." << endl; 
      string special_action_cmd;  
      cin >> special_action_cmd; 

      special_action_cmd = ci.parseCommands(special_action_cmd, times);

      if (special_action_cmd == "blind") {
        currboard = boards.at(curr_player);
        currboard->setBlind();

      } else if (special_action_cmd == "heavy") {
        currboard = boards.at(curr_player);
        currboard->setHeavy();

      } else if (special_action_cmd == "force") {
          string force_to;
          cin >> force_to;
          executeCmd(1, force_to);
      }
    }

    if (!(boards.at((curr_player + 1) % (num_players)))->gameOver()) {
      curr_player = (curr_player + 1) % (num_players);
    }
    boards.at(curr_player)->setRowsCleared();

  } else if (cmd == "levelup") {
    if (currboard->getLevel() + times <= 4) {
      currboard->levelup(times);
      currboard->setRandom();
      currboard->setLevelPointer(); 
    }

  } else if (cmd == "leveldown") {
    if (currboard->getLevel()- times >= 0) {
      currboard->leveldown(times); 
      currboard->setRandom();
      currboard->setLevelPointer(); 
    }
    
  }  else if (times == 1) {
    //commands that can only have 1 time

    if (cmd == "norandom" && (boards.at(curr_player))->getLevel() >= 3) {
      string fname; 
      cin >> fname; 
      (boards.at(curr_player))->resetSeq(fname); 
      currboard->unsetRandom();
      currboard->setLevelPointer(); 

    } else if (cmd == "random" && (boards.at(curr_player))->getLevel() >= 3) {
      currboard->setRandom();
      currboard->setLevelPointer(); 

    } else if (cmd == "I"){
      currboard->setCurrBlock(currboard->getLevel(), 'I'); 

    } else if (cmd == "J"){
      currboard->setCurrBlock(currboard->getLevel(), 'J');

    } else if (cmd == "L"){
      currboard->setCurrBlock(currboard->getLevel(), 'L');

    } else if (cmd == "O"){
      currboard->setCurrBlock(currboard->getLevel(), 'O'); 
      
    } else if (cmd == "T"){
      currboard->setCurrBlock(currboard->getLevel(), 'T');    

    } else if (cmd == "S"){
      currboard->setCurrBlock(currboard->getLevel(), 'S'); 

    } else if (cmd == "Z"){
      currboard->setCurrBlock(currboard->getLevel(), 'Z'); 

    } else if (cmd == "restart") {
      restart(); 
      
    } else if (cmd == "help"){
      ci.printCommands();
    }
  } else {
    cin.ignore(); 
    cin.clear(); 
    cout << "Invalid. Type 'help' for list of valid commands." << endl; 
  }

  if (boards.at(curr_player)->getDropped()) {
    boards.at(curr_player)->toggleDropped();
    if (!(boards.at((curr_player + 1) % (num_players)))->gameOver()) {
      curr_player = (curr_player + 1) % (num_players);
    }
  }
}

bool Biquadris::allGamesOver(){
  for (size_t i = 0; i < boards.size(); i++){
    if (!(boards.at(i)->gameOver())){
      return false; 
    }
  }
  return true; 
}
void Biquadris::printGame(){ 
   for (size_t i = 0; i < boards.size(); i++){
    if (boards.at(i)->gameOver()){
       boards.at(i)->changeDrops(0);
    }
   }

  if (allGamesOver()){
    cout << "Game Over" << endl; 
    return; 
  }

  cout << endl; 
  // print text above boards 
  cout << left << setw(board_cols - to_string(boards.at(0)->getLevel()).length()) << "Level: " << right << boards.at(0)->getLevel()   
       << setw(space_between_boards -1) << "" 
       << left << setw(board_cols - to_string(boards.at(1)->getLevel()).length()) << "Level: " << right << boards.at(1)->getLevel() 
       << endl; 

  cout << left << setw(board_cols - to_string(boards.at(0)->getScore()).length()) << "Score: " << right << boards.at(0)->getScore()   
       << setw(space_between_boards-1) << ""
       << left << setw(board_cols - to_string(boards.at(1)->getScore()).length()) << "Score: " << right << boards.at(1)->getScore() 
       << endl; 

  cout << left << string(board_cols, '-') 
        << right << setw(space_between_boards+board_cols-1) << string(board_cols, '-') 
        << endl;

  for (size_t i = 0; i < board_rows; i++){
    boards.at(0)->printRow(i);
    cout << setw(space_between_boards); 
    boards.at(1)->printRow(i); 
    cout << endl; 
  }
  
  cout << left << string(board_cols, '-') << right << setw(space_between_boards+board_cols-1) << string(board_cols, '-') << endl;

  cout << "Next:" << setw(space_between_boards+board_cols-1) << "Next:" << endl; 

  for (size_t i = 2; i < 4; i++){
    boards.at(0)->getNextBlock()->printRow(i, board_cols);
    cout << setw(space_between_boards); 
    boards.at(1)->getNextBlock()->printRow(i, board_cols); 
    cout << endl; 
  }
  cout << endl; 

}

void Biquadris::displayTextOnly(){
  this->onlyText = true; 
}

void Biquadris::setSeed(int seed){
  this->seed = seed; 
}

void Biquadris::setStartLevel(int start_level){
  this->start_level = start_level;
}

void Biquadris::setScript1(string fname){
  this->scriptfilen.at(0) = fname; 
}

void Biquadris::setScript2(string fname){
  this->scriptfilen.at(1) = fname; 
}

void Biquadris::populateScripts() {
  char scriptBlock;

  ifstream sfile;
  sfile.open(this->scriptfilen.at(0));

  while (true) {
    if (sfile.eof()) {
      break;
    }
    if (sfile >> scriptBlock) {
      (this->p1p2seq.at(0)).emplace_back(scriptBlock);
    }
  }
  sfile.close();

  ifstream sfile2;
  sfile2.open(this->scriptfilen.at(1));

  while (true) {
    if (sfile2.eof()) {
      break;
    }
    if (sfile2 >> scriptBlock) {
      (this->p1p2seq.at(1)).emplace_back(scriptBlock);
    }
  }
  sfile2.close();
}

Biquadris::~Biquadris() {
  for (size_t i = 0; i < boards.size(); i++) {
    delete boards.at(i);
  }
}




