#include "level1.h"
#include <cstdlib> 

using namespace std; 
LevelOne::LevelOne(int seed)
    : Level{1}, seed{seed}{}

Block * LevelOne::genBlock(){
        // produces a number between 1 and 12
    int rand_num = (rand() % 12) + 1;  
    Block* new_block;
    if (rand_num == 1){
        new_block = new SBlock(level);
        return new_block;
    } else if (rand_num == 2){
        new_block = new ZBlock(level);
        return new_block;
    } else if (rand_num == 3 || rand_num == 4){
        new_block = new OBlock(level);
        return new_block;
    } else if (rand_num == 5 || rand_num == 6){
        new_block = new LBlock(level);
        return new_block;
    } else if (rand_num == 7 || rand_num == 8){
        new_block = new JBlock(level);
        return new_block;
    } else if (rand_num == 9 || rand_num == 10){
        new_block = new TBlock(level);
        return new_block;
    } else if (rand_num == 11 || rand_num == 12){
        new_block = new IBlock(level);
        return new_block;
    }

    return NULL;
}




