#ifndef LEVELTWO_H
#define LEVELTWO_H
#include "level.h"

// Implements Level 
class LevelTwo : public Level {
	// probabilities of each piece 
	const float prob_S = 1/7; 
	const float prob_Z = 1/7; 
	const float prob_O = 1/7; 
	const float prob_L = 1/7; 
	const float prob_J = 1/7; 
	const float prob_T = 1/7; 
	const float prob_I = 1/7; 
	int seed = time(NULL); // default 

	public: 
		LevelTwo(int seed);
		virtual Block * genBlock() override; 
}; 

#endif 





