#include "sblock.h"
using namespace std; 
 
SBlock::SBlock(int level) : Block{level, 'S'} {
    abs_pos.emplace_back(Coord{1, 3});
    abs_pos.emplace_back(Coord{0, 3});
    abs_pos.emplace_back(Coord{1, 2});
    abs_pos.emplace_back(Coord{2, 2});
}



