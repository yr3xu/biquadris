#ifndef COORD_H
#define COORD_H
#include <iostream>
#include <vector>

class Coord {
    public: 
        int x;      
        int y;

        Coord(int x = 0, int y = 0);
        bool isIn(std::vector<Coord>& v);
        bool isLargestY(std::vector<Coord>& v);
        bool isLargestX(std::vector<Coord>& v);
        bool isSmallestX(std::vector<Coord>& v);
        int getMaxX(std::vector<Coord> v);
        int getMaxY(std::vector<Coord> v);
        int getMinX(std::vector<Coord> v);
        int getMinY(std::vector<Coord> v); 
        Coord rot_cw_around(Coord orig, Coord around);   // rotates "orig" 90 degrees clockwise around "around"
};

#endif





