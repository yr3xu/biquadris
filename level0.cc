#include "level0.h"

LevelZero::LevelZero(std::vector<char> sequence)
    : Level{0}, sequence{sequence}{}

Block * LevelZero::genBlock(){ 
    char type = sequence.at(counter);
    counter = (counter + 1) % sequence.size(); // loop through if finished reading next time  
    switch(type){
        case 'S':
            return new SBlock(0); 
        case 'Z':
            return new ZBlock(0); 
        case 'O':
            return new OBlock(0); 
        case 'T':
            return new TBlock(0); 
        case 'J':
            return new JBlock(0); 
        case 'I':
            return new IBlock(0); 
        case 'L':
            return new LBlock(0); 
        default:
            return NULL;
    }
}

LevelZero::~LevelZero() {};





