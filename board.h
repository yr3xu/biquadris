#ifndef BOARD_H
#define BOARD_H
#include <iostream>
#include <vector>
#include <cstddef>
#include <memory>
#include "cell.h"
#include "info.h"
#include "block.h" 
#include "level.h"
#include "level0.h" 
#include "level1.h"
#include "level2.h"
#include "level3.h"
#include "level4.h"
#include "textdisplay.h"
#include "observer.h"
#include "subject.h"
#include "lblock.h"
#include "sblock.h"
#include "oblock.h"
#include "tblock.h"
#include "jblock.h"
#include "zblock.h"
#include "coord.h"
#include "graphicsdisplay.h"

class TextDisplay;
class GraphicsDisplay; 
template <typename Info> class Observer;

class Board : public Subject<Info> { 
 protected: 
  //board
  std::vector<std::vector<Cell>> theBoard;  // The actual grid.
  TextDisplay *td = nullptr; // The text display.
  // std::shared_ptr<TextDisplay> td = nullptr; // The text display.
  Observer<Info> *ob = nullptr;

  // blocks
  Block* currBlock;
  Block* nextBlock;
  std::vector<Block*> activeBlocks;
  std::vector<char> block_seq; // blocks for level 0

  // levels-related stuff (next block / scoring ) 
  int seed = time(NULL);       //possibly remove after
  Level *l; 
  int level;
  int remainingDrops = 0;
  int fork_counter = 0;

  // game status 
  int score = 0; 
  bool isDropped = false;         
  bool lost = false; 
  bool heavy = false;
  bool only_text = false;
  int rowsCleared = 0; 
  bool isBlind = false; 
  bool initBlind = false;
  bool isRandom = true; 


  // helper methods
  bool inBounds(int c, int r, int max_width, int max_height);
  bool isEmptyCell(int r, int c); 
  bool canMoveTo(std::vector<Coord> c);      // if all 4 coordinates are empty and in bounds, then valid move. 
  bool rowFull(int r); 
  void checkFullRows();
  void collapseRows(int r, std::vector<int>& validator);              // shifts board down by one row
  void clearRow(int r);                  // removes any full blocks from bottom of board. called by dropBlock()

 public:

  Info getInfo() const override;
  Board(int start_level = 0); 
  virtual ~Board();

  //initializing and printing 
  void init(size_t board_cols, size_t board_rows, bool only_text); // Sets up an 18x11 board, and clears old grid if necessary.  
  virtual void generateNewBlock();                  // gets the block when initialized, // rename genBlock()
  
  // block movement (virtual for decorator)
  virtual void moveRight(size_t times = 1);   // shift block Right by "times" cells 
  virtual void moveLeft(size_t times = 1);   // shift block left by "times" cells 
  virtual void moveDown(size_t times = 1);    // shift block down by "times" cells 
  virtual void rotateCw(size_t times = 1); 
  virtual void rotateCcw(size_t times = 1); 

  // other user commands
  virtual void levelup(size_t times=1); 
  virtual void leveldown(size_t times=1); 
  virtual void dropBlock();               // drop block to bottom of board 

  //printing    
  void printRow(size_t r);       // print row r of theBoard - only biquadris should be able to use this 

  // getters and setters   
  int getLevel() const;     // getter for level 
  int getScore() const;     // getter for score 
  Block * getNextBlock() const;   // needed for printing board in biquadris 
  bool isHeavy() const;
  void toggleHeavy();
  void setHeavy();
  bool getDropped() const;
  bool toggleDropped();

  bool gameOver() const; 
  void setCurrBlock(int level, char type);   //sets AND DRAWS CurrBlock on board
  void setLevelPointer(); 
  void setSeq(std::vector<char> seq);
  void resetSeq(std::string fname);
  void changeDrops(int changeTo);
  int getDrops();
  int getRowsCleared();
  void setRowsCleared();     //returns the number of rows cleared after the block was dropped (used for blind)
  void setBlind();
  void unsetBlind(); 

  void setRandom();
  void unsetRandom(); 

  // potential decorator overrides 
  void draw(Block* b);
  virtual void undraw(Block* b);

 
};

#endif




