#ifndef LEVELZERO_H
#define LEVELZERO_H
#include "level.h"

// Implements Level 
class LevelZero : public Level {
	int counter = 0; 		
	std::vector <char> sequence; 
	public: 
		~LevelZero();
		LevelZero(std::vector<char> sequence); 
		virtual Block * genBlock() override; 
	
}; 

#endif 



