
#include "zblock.h"
using namespace std; 
 
ZBlock::ZBlock(int level) : Block{level, 'Z'} {
    abs_pos.emplace_back(Coord{1, 3});
    abs_pos.emplace_back(Coord{0, 2});
    abs_pos.emplace_back(Coord{1, 2});
    abs_pos.emplace_back(Coord{2, 3});
}



