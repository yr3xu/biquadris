#ifndef CELL_H
#define CELL_H
#include <cstddef>    // needed? 
#include "subject.h"
#include "observer.h"
#include "info.h"
#include "block.h"
#include "coord.h"


class Cell : public Subject<Info>, public Observer<Info> {
  int level;
  const size_t r, c;
  char type;
  bool isSet;
  bool detachObs;
  bool isBoard = false;
  Block* block;
  
 public:
  Cell(size_t r, size_t c);
  void setCell(Info info);    
  void notify(Subject<Info> &whoFrom) override;    // My neighbours will call this when they've changed state
  Info getInfo() const override;
};
#endif




