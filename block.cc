#include "block.h"
#include "coord.h" // remove

using namespace std; 
class Coord; 

Block::Block(int level, char type)
    : level{level}, type{type} {
}

/**** Temp ****/
int getMaxY(vector <Coord> v){
  int max = v.front().y; 
  for (size_t i = 1; i < v.size(); i++){
    if (v.at(i).y > max){
      max = v.at(i).y; 
    }
  }
  return max; 
}

int getMaxX(vector <Coord> v){
  int max = v.front().x; 
  for (size_t i = 1; i < v.size(); i++){
    if (v.at(i).x > max){
      max = v.at(i).x; 
    }
  }
  return max; 
}

int getMinX(vector <Coord> v){
  int min = v.front().x; 
  for (size_t i = 1; i < v.size(); i++){
    if (v.at(i).x < min){
      min = v.at(i).x; 
    }
  }
  return min; 
}

int getMinY(vector <Coord> v){
  int min = v.front().y; 
  for (size_t i = 1; i < v.size(); i++){
    if (v.at(i).y < min){
      min = v.at(i).y; 
    }
  }
  return min; 
}

Coord rot_cw_around(Coord orig, Coord around){
  return Coord{-(orig.y-around.y) + around.x, (orig.x-around.x) + around.y}; 
}
/********* Block movement *******/ 

void Block::moveRight() {
    x_coord += 1;
    for (size_t i = 0; i < abs_pos.size(); i++) {
        abs_pos.at(i).x += 1;
    }
}

void Block::moveLeft() {
    x_coord -= 1;
    for (size_t i = 0; i < abs_pos.size(); i++) {
        abs_pos.at(i).x -= 1;
    }
}

void Block::moveDown() {
    y_coord += 1;
    for (size_t i = 0; i < abs_pos.size(); i++) {
        abs_pos.at(i).y += 1;
    }
}

// shift left by (rightmost - leftmost) units 
// rotate about LL corner (LLx, LLy)
void Block::rotateCw(){
    vector <Coord> copy = abs_pos; 
    Coord around {getMinX(abs_pos), getMaxY(abs_pos)}; 
    for (size_t i = 0; i < 4; i++){
        abs_pos.at(i).x -= getMaxX(copy) - getMinX(copy); 
        abs_pos.at(i) = rot_cw_around(abs_pos.at(i), around);
    }
}

void Block::rotateCcw(){
    for (int i = 0 ; i < 3; i++){
        rotateCw(); 
    }
}

/******** Getters/Setters ********/ 
Coord Block::getCoords() {
    return Coord{x_coord, y_coord};
}

void Block::setCoords(int x, int y) {
    x_coord = x; 
    y_coord = y;
}

vector<Coord> Block::getAbsoluteLoc() {
  return abs_pos;
}


char Block::getType() {
    return type;
}

void Block::setAbsPos(vector<Coord> pos){
    abs_pos = pos; 
}

void Block::removeCoords(Coord& location) {
  for (int i = 0; i < abs_pos.size(); i++) {
    if (abs_pos.at(i).x == location.x && abs_pos.at(i).y == location.y) {
      abs_pos.erase(abs_pos.begin() + i);
    }
  }
}

int Block::getLevel() const{
  return level;
}

void Block::printRow(int r, int board_width){
  for (int i = 0; i < board_width; i++){
    bool found = false; 
    for (int j = 0; j < 4; j++){
      if (this->abs_pos.at(j).y == r && this->abs_pos.at(j).x == i){
          cout << type; 
          found = true; 
          break; 
      }
    }
    if (!found){
        cout << " "; 
      } 
  }
}




