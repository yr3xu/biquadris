#ifndef LEVELFOUR_H
#define LEVELFOUR_H
#include "level.h"
#include "level3.h"

// Implements Level 
class LevelFour : public Level {
	// probabilities of each piece 
	const float prob_S = 2/9; 
	const float prob_Z = 2/9; 
	const float prob_O = 1/9; 
	const float prob_L = 1/9; 
	const float prob_J = 1/9; 
	const float prob_T = 1/9; 
	const float prob_I = 1/9; 
	int seed = time(NULL); // default 

	int counter = 0; 
	std::vector <char> sequence; 
	bool isRandom;

	public:
		LevelFour(int seed, std::vector<char> sequence, bool isRandom); 
		virtual Block* genBlock() override; 
}; 

#endif 




