#include "graphicsdisplay.h"
#include "subject.h"
using namespace std;

GraphicsDisplay::GraphicsDisplay(size_t board_cols, size_t board_rows)
  {
  xw.fillRectangle(0, 0, xw_width/2*0.95, xw_width, Xwindow::White); 

  xw.drawString(xw_width/2*0.05, 30, "Level: 0"); 
  xw.drawString(xw_width/2*0.05, 45, "Score: 0"); 
  xw.drawString(xw_width/2*1.2, 45, "Next Piece: "); 
  xw.drawString(xw_width/2*0.05, 60, "-----------"); 

  for (size_t i = 0; i < num_cols; i++) {
    for (size_t j = 0; j < num_rows; j++) {
      xw.fillRectangle(i * cell_width, j * cell_height + stats_offset, cell_width, cell_height, Xwindow::Black); 
      xw.fillRectangle(i * cell_width + 1, j * cell_height + 1 + stats_offset, cell_width-2, cell_height-2, Xwindow::Linen); 
    }
  }

  for (size_t i = 0; i < num_rows; i++){
    vector <char> dp_row; 
    for (size_t j = 0; j < num_cols; j++){
      dp_row.emplace_back('.');
    }
    theDisplay.emplace_back(dp_row);
  }
} 

Info GraphicsDisplay::getInfo() const {
    return Info{-1, 0, 0, '.', false, false, false, nullptr}; 
} 


int GraphicsDisplay::getColor(char type){ 
  if (type == 'S'){
    return Xwindow::Green; 
  }  else if (type == 'O'){
    return Xwindow::Yellow; 
  } else if (type == 'T'){
    return Xwindow::Purple; 
  } else if (type == 'J'){
    return Xwindow::Blue; 
  } else if (type == 'L'){
    return Xwindow::Orange; 
  } else if (type == 'I'){
    return Xwindow::Cyan; 
  } else if (type == 'Z'){
    return Xwindow::Red; 
  } else if (type == '*'){
    return Xwindow::Gray; 
  } else {
    return Xwindow::Linen; 
  }
}

// map the row and column numbers of a given cell object to the corresponding coordinates of the squares in the window.
void GraphicsDisplay::notify(Subject<Info> &whoNotified){
  int row;
  int col;
  char type;

  if (whoNotified.getInfo().type == 'B') {
    xw.fillRectangle(xw_width/2*0.05, 15, 100, 50, Xwindow::White);
    xw.drawString(xw_width/2*0.05, 30, "Level: " + to_string(whoNotified.getInfo().level));   
    xw.drawString(xw_width/2*0.05, 45, "Score: " + to_string(whoNotified.getInfo().row)); 
    xw.fillRectangle(xw_width/2*1.2, 30, 100, 500, Xwindow::White); 

    vector<Coord> abs = whoNotified.getInfo().block->getAbsoluteLoc();
    for (size_t i = 0; i < abs.size(); i++) {
      int col = abs.at(i).x;
      int row = abs.at(i).y;
      xw.drawString(xw_width/2*1.2, 45, "Next Piece: "); 
      xw.fillRectangle(col * cell_width + 300, row * cell_height + stats_offset, cell_width, cell_height, Xwindow::Black); 
      xw.fillRectangle(col * cell_width + 301, row * cell_height + 1 + stats_offset, cell_width-2, cell_height-2, getColor(whoNotified.getInfo().block->getType()));
    }


  } else {
    row = whoNotified.getInfo().row;
    col = whoNotified.getInfo().col; 
    type = whoNotified.getInfo().type;
    theDisplay.at(row).at(col) = type;
    xw.fillRectangle(col * cell_width, row * cell_height + stats_offset, cell_width, cell_height, Xwindow::Black); 
    xw.fillRectangle(col * cell_width + 1, row * cell_height + 1 + stats_offset, cell_width-2, cell_height-2, getColor(type)); 
  }
  if (whoNotified.getInfo().type == 'B' 
      && whoNotified.getInfo().isBlind == true
      && whoNotified.getInfo().detachObs == false
      && whoNotified.getInfo().isSet == false) { 
    blind = true;
    
  } 
  
  if (blind) {
    for (int i = 3-1; i < 12-1; i++) {  //row
      for (int j = 3-1; j < 9-1; j++) { //col
        xw.fillRectangle(j * cell_width, i * cell_height + stats_offset, cell_width, cell_height, Xwindow::Black); 
        xw.fillRectangle(j * cell_width, i * cell_height + stats_offset, cell_width - 2, cell_height - 2, Xwindow::White);  
      }
    }
  }
}




