#include "iblock.h"
using namespace std; 
 
IBlock::IBlock(int level) : Block{level, 'I'} {
    x_coord = 1;
    y_coord = 3;

    abs_pos.emplace_back(Coord{1, 3});
    abs_pos.emplace_back(Coord{0, 3});
    abs_pos.emplace_back(Coord{2, 3});
    abs_pos.emplace_back(Coord{3, 3});
}




