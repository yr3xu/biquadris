
#include "oblock.h"
using namespace std; 
 
OBlock::OBlock(int level) : Block{level, 'O'} {
    abs_pos.emplace_back(Coord{1, 3});
    abs_pos.emplace_back(Coord{0, 3});
    abs_pos.emplace_back(Coord{0, 2});
    abs_pos.emplace_back(Coord{1, 2});
}



