#include "level2.h"

LevelTwo::LevelTwo(int seed)
    : Level{2}, seed{seed}{}

Block * LevelTwo::genBlock(){
    // produces a number between 1 and 7
    int rand_num = (rand() % 7) + 1;  
    Block* new_block;
    if (rand_num == 1){
        new_block = new SBlock(level);
        return new_block;
    } else if (rand_num == 2){
        new_block = new ZBlock(level);
        return new_block;
    } else if (rand_num == 3){
        new_block = new OBlock(level);
        return new_block;
    } else if (rand_num == 4){
        new_block = new LBlock(level);
        return new_block;
    } else if (rand_num == 5){
        new_block = new JBlock(level);
        return new_block;
    } else if (rand_num == 6){
        new_block = new TBlock(level);
        return new_block;
    } else if (rand_num == 7){
        new_block = new IBlock(level);
        return new_block;
    }

    return NULL;
}




