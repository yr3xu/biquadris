#include "coord.h"
using namespace std; 

Coord::Coord(int x, int y) : x{x}, y{y} {}

bool Coord::isIn(vector<Coord> &v) {
    for(size_t i = 0; i < v.size(); i++) {
        if (this->x == v.at(i).x && this->y == v.at(i).y) {
            return true;
        }
    }
    return false;
}

bool Coord::isLargestY(vector<Coord> &v) {
    for(size_t i = 0; i < v.size(); i++) {
        if (this->x == v.at(i).x && this->y < v.at(i).y) {
            return false;
        }
    }
    return true;
}

bool Coord::isLargestX(vector<Coord>& v) {
    for(size_t i = 0; i < v.size(); i++) {
        if (this->y == v.at(i).y && this->x < v.at(i).x) {
            return false;
        }
    }
    return true;
}

bool Coord::isSmallestX(vector<Coord>& v) {
    for(size_t i = 0; i < v.size(); i++) {
        if (this->y == v.at(i).y && this->x > v.at(i).x) {
            return false;
        }
    }
    return true;
}

int Coord::getMaxY(vector <Coord> v){
  int max = v.back().y; 
  for (size_t i = 1; i < v.size(); i++){
    if (v.at(i).y > max){
      max = v.at(i).y; 
    }
  }
  return max; 
}

int Coord::getMaxX(vector <Coord> v){
  int max = v.back().x; 
  for (size_t i = 1; i < v.size(); i++){
    if (v.at(i).x > max){
      max = v.at(i).x; 
    }
  }
  return max; 
}

int Coord::getMinX(vector <Coord> v){
  int min = v.back().x; 
  for (size_t i = 1; i < v.size(); i++){
    if (v.at(i).x < min){
      min = v.at(i).x; 
    }
  }
  return min; 
}

int Coord::getMinY(vector <Coord> v){
  int min = v.back().y; 
  for (size_t i = 1; i < v.size(); i++){
    if (v.at(i).y < min){
      min = v.at(i).y; 
    }
  }
  return min; 
}

Coord Coord::rot_cw_around(Coord orig, Coord around){
  return Coord{-(orig.y-around.y) + around.x, (orig.x-around.x) + around.y}; 
}




