CXX = g++
CXXFLAGS= -std=c++14 -Wall -MMD -Werror=vla 
EXEC = biquadris
OBJECTS = main.o biquadris.o board.o cell.o block.o iblock.o jblock.o lblock.o oblock.o sblock.o zblock.o tblock.o level.o level1.o level2.o level3.o level4.o level0.o info.o observer.o subject.o textdisplay.o  window.o coord.o commandinterpreter.o xblock.o graphicsdisplay.o
DEPENDS = ${OBJECTS:.o=.d}

${EXEC}:${OBJECTS} 
	${CXX} ${CXXFLAGS} ${OBJECTS} -o ${EXEC} -lX11 

-include ${DEPENDS}

.PHONY: clean

clean:
	rm ${OBJECTS} ${DEPENDS}

