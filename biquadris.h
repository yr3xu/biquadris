#ifndef BIQUADRIS_H
#define BIQUADRIS_H
#include <vector>
#include <map>
#include "board.h"
#include "commandinterpreter.h"

/********* The public interface of the 2-player Biquadris game  ***********/ 

class Biquadris {
private:
  const size_t board_rows = 18;
  const size_t board_cols = 11;
  const int space_between_boards = 10;                                
  const int longest_row_width = 2 * board_cols + space_between_boards;  
  const std::map<std::string, std::string> cmds =
      { {"rename", "rename"},
        {"left", "left"},
       {"right", "right"},
       {"down", "down"},
       {"clockwise", "clockwise"},
       {"counterclockwise", "counterclockwise"},
       {"drop", "drop"},
       {"levelup", "levelup"},
       {"leveldown", "leveldown"},
       {"norandom", "norandom"},
       {"random", "random"},
       {"sequence", "sequence"},
       {"I", "I"},
       {"J", "J"},
       {"L", "L"}, 
       {"O", "O"},
       {"S", "S"},
       {"Z", "Z"},
       {"T", "T"},
       {"restart", "restart"},
       {"blind", "blind"},
       {"force", "force"},
       {"heavy", "heavy"},
       {"help", "help"} }; 

protected:
  std::vector<Board *> boards;
  std::vector<int> drops;     // the number of times left that drop() has to be executied for each board 
  CommandInterpreter ci {cmds}; 
  size_t highscore = 0;
  size_t curr_player = 0;   // which player's turn it is. player number starts at 0; i.e player 0, player 1

  // arguments may overwrite  these default values 
  size_t num_players = 2;  
  int start_level = 0;     //  all players start at this level at the beginning 
  bool onlyText = false;
  bool isHeavy = false;
  int seed = time(NULL); // for levels 2-4 generating blocks  
  std::vector<std::string> scriptfilen = {"sequence1.txt", "sequence2.txt"};

  std::vector<std::vector<char>> p1p2seq = { {}, {} };

  //setters (for args)
  void setSeed(int seed);
  void setStartLevel(int startLevel);
  void displayTextOnly();
  void setScript1(std::string fname);
  void setScript2(std::string fname);
  void populateScripts();

  //setters (for mcds)

  // protected methods 
  void init(); 
  bool allGamesOver(); 
  void restart();            //same # of players, same args
  void printGame();  
  void readArgs(int argc, char *argv[]);
  void readCmd();
  void executeCmd(int times, std::string cmd);

public:
  ~Biquadris();
  void play(int num_players, int argc, char *argv[]);

};

#endif





