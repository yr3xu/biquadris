#include "level4.h"

LevelFour::LevelFour(int seed, std::vector<char> sequence, bool isRandom)
    : Level{4}, seed{seed}, sequence{sequence}, isRandom{isRandom} {}

Block * LevelFour::genBlock(){
    
    if (!isRandom) {
        char type = sequence.at(counter);
        counter = (counter+1) % sequence.size(); // loop through if finished reading next time  
         switch(type){
            case 'S':
                return new SBlock(0); 
            case 'Z':
                return new ZBlock(0); 
            case 'O':
                return new OBlock(0); 
            case 'T':
                return new TBlock(0); 
            case 'J':
                return new JBlock(0); 
            case 'I':
                return new IBlock(0); 
            case 'L':
                return new LBlock(0); 
            default:
                return NULL;
        }
    }

    else {
        int rand_num = (rand() % 9) + 1;  
        Block* new_block;
        if (rand_num == 1 || rand_num == 2){
            new_block = new SBlock(level);
            return new_block;
        } else if (rand_num == 3 || rand_num == 4){
            new_block = new ZBlock(level);
            return new_block;
        } else if (rand_num == 5){
            new_block = new OBlock(level);
            return new_block;
        } else if (rand_num == 6){
            new_block = new LBlock(level);
            return new_block;
        } else if (rand_num == 7){
            new_block = new JBlock(level);
            return new_block;
        } else if (rand_num == 8){
            new_block = new TBlock(level);
            return new_block;
        } else if (rand_num == 9){
            new_block = new IBlock(level);
            return new_block;
        }

        return NULL;
    }
}




