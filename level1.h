#ifndef LEVELONE_H
#define LEVELONE_H
#include "level.h"

// Implements Level 

class LevelOne : public Level {
	// probabilities of each piece 
	const float prob_S = 1/12; 
	const float prob_Z = 1/12; 
	const float prob_O = 1/6; 
	const float prob_L = 1/6; 
	const float prob_J = 1/6; 
	const float prob_T = 1/6; 
	const float prob_I = 1/6; 
	int seed = time(NULL); // default 

	public: 
		LevelOne(int seed);
		virtual Block * genBlock() override; 
}; 

#endif 




