#ifndef BLOCK_H
#define BLOCK_H
#include <iostream>
#include <vector>
#include <cstddef>
#include "coord.h"

class TextDisplay;
class GraphicsDisplay; 
class Coord; 

class Block {
 protected: 

    int x_coord = 1;       
    int y_coord = 3; 
    int level; 
    char type;

    std::vector<Coord> abs_pos;     
 
 public:
    Block(int level, char type); 

   // movement 
    void moveRight();
    void moveLeft();
    void moveDown();
    void rotateCw();
    void rotateCcw();
    void drop();      

    // getters and setters
    Coord getCoords(); 
    void setCoords(int x, int y);
    char getType();
    int getLevel() const;
    std::vector<Coord> getAbsoluteLoc();   
    void setAbsPos(std::vector<Coord> pos); 
    void removeCoords(Coord& location); 
    void printRow(int r, int board_width); 
};

#endif





