#ifndef LEVELTHREE_H
#define LEVELTHREE_H
#include "level.h"
// Implements Level 

class LevelThree : public Level {
	// probabilities of each piece 
	const float prob_S = 2/9; 
	const float prob_Z = 2/9; 
	const float prob_O = 1/9; 
	const float prob_L = 1/9; 
	const float prob_J = 1/9; 
	const float prob_T = 1/9; 
	const float prob_I = 1/9; 
	int seed = time(NULL); // default 

	int counter = 0; 
	std::vector <char> sequence; 
	bool isRandom;
	
	public: 
		LevelThree(int seed, std::vector<char> sequence, bool isRandom);
		virtual Block * genBlock() override; 
}; 

#endif 



